# deserialization
library("rjson")
json_data <- fromJSON(paste(readLines("test.json"), collapse = ""))
json_data$table1$data$code[1]

# serialization
json_str <- toJSON(json_data)
cat(json_str)